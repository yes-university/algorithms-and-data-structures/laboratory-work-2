const { shuffle, arrayGen } = require("./array-gen");
const bubbleSort = require("./bubbleSort");
const insertionSort = require("./insertSort");
const selectionSort = require("./selectSort");
const shakerSort = require("./shakerSort");

const addonNumber = 3;

const array = shuffle(arrayGen(10000 * addonNumber, 1, 20 * addonNumber));

console.log("Shuffle array: ", array);
(async () => {
  const { default: prettyBytes } = await import("pretty-bytes");

  function testSort(sort, arr, sortType = "none") {
    const defaultSortResult = [...arr].sort((a, b) => a - b);
    const startUsed = process.memoryUsage().heapUsed;
    const start = performance.now();
    const sortResult = sort(arr);
    const end = performance.now();
    const endUsed = process.memoryUsage().heapUsed;

    const sortAcc = defaultSortResult.toString() === sortResult.toString();
    console.log(
      `Sort type: ${sortType} - ${sortAcc ? "OK" : "FAIL"}`,
      `Time: ${(end - start).toFixed(4)}ms`,
      `Memory: ${prettyBytes(endUsed - startUsed)}`
    );
    if (!sortAcc) {
      console.log({
        defaultSortResult,
        sortResult,
      });
    }
  }

  testSort(selectionSort, [...array], "selection");
  testSort(bubbleSort, [...array], "bubble");
  testSort(shakerSort, [...array], "shaker");
  testSort(insertionSort, [...array], "insertion");
})();
