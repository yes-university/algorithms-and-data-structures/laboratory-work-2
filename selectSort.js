module.exports = function selectionSort(arr) {
  let iterations = 0;
  for (let i = 0; i < arr.length; i++) {
    let minIndex = i;
    let min = arr[minIndex];
    for (let j = i + 1; j < arr.length; j++) {
      //   console.log("Iteration:", iterations, "Array:", JSON.stringify(arr));
      iterations++;
      if (arr[j] < min) {
        minIndex = j;
        min = arr[j];
      }
    }
    if (minIndex !== i) {
      arr[minIndex] = arr[i];
      arr[i] = min;
    }
  }
  return arr;
};
