module.exports = function insertionSort(arr) {
  let iterations = 0;
  for (let i = 1; i < arr.length; i++) {
    let current = arr[i];
    let j = i - 1;
    while (j >= 0 && current < arr[j]) {
      //   console.log("Iteration:", iterations, "Array:", JSON.stringify(arr));
      iterations++;
      arr[j + 1] = arr[j];
      j--;
    }
    arr[j + 1] = current;
  }
  return arr;
};
