function arrayGen(len, min, max) {
  const arr = [];
  for (var i = 0; i < len; i++) {
    arr.push(Math.floor(Math.random() * (max - min + 1)) + min);
  }
  return arr;
}

function shuffle(arr) {
  for (let i = 0; i < arr.length; i++) {
    let random = Math.floor(Math.random() * arr.length);
    [arr[i], arr[random]] = [arr[random], arr[i]];
  }
  return arr;
}

module.exports = { arrayGen, shuffle };
