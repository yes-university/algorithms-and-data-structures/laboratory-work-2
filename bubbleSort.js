module.exports = function bubbleSort(arr) {
  let iterations = 0;
  let swapped = true;
  while (swapped) {
    // console.log("Iteration:", iterations, "Array:", JSON.stringify(arr));
    iterations++;
    swapped = false;
    for (let i = 1; i < arr.length; i++) {
      const prev = arr[i - 1];
      const current = arr[i];
      if (prev > current) {
        arr[i] = prev;
        arr[i - 1] = current;
        swapped = true;
      }
    }
  }
  return arr;
};
